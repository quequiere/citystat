from collections import defaultdict
from math import *




# -------- CLASS DEFINITION -------------------------------------------------------------------------------------------------------------------------------------

class Habitant(object):
    def __init__(self,nom,prenom,habitations,age,nbVoitures,estHomme,estProprietaire) :
        self.nom = nom
        self.prenom = prenom
        #Retourne toutes les habitations et si proprietaire ou non
        self.habitations = habitations
        self.age = age
        #Retourne retourne toutes les voitures et si proprietaire ou non
        self.nbVoitures = nbVoitures
        self.estHomme = estHomme
        self.estProprietaire = estProprietaire
        
    def __str__(self):
         #nom prenom a 10 habitation(s). Elle a l'age de 60 ans et a 20 voiture(s). Elle est proprietaire de son logement
        return "%s %s a %s habitation(s) %s a l'age de %s ans et a %s voiture(s). %s est %s de son logement" % (self.nom,self.prenom, len(self.habitations),self.getPronom(),self.age, self.nbVoitures, self.getPronom(), self.getProprietaire())

    def getPronom(self):
        if self.estHomme:
            return "il"
        else :
            return "elle"

    def getProprietaire(self):
        if self.estProprietaire:
            return "proprietaire"
        else :
            return "locataire"

    def getHabitationsBatiment(self,batiments):
        return filter(lambda b: b.id in self.habitations, batiments)

    

class Batiment(object):
    def __init__(self,id,rue,codePostal,nombrePiece,estAppartement,estPublic,superficie,typeBatiment) :
        self.id = id
        self.rue = rue
        self.codePostal = codePostal
        self.nombrePiece = nombrePiece
        #Est un appartement ou une maison
        self.estAppartement = estAppartement
        #Est publique ou prive
        self.estPublic = estPublic
        #Superficie en MC
        self.superficie = superficie
        self.typeBatiment = typeBatiment

    def __str__(self):
         #Appartement 0 situe avenue du chat 75003 avec 5 pieces d'une superficie de 5 mc d'appartenance privee
        return "%s %s situe %s %s avec %s piece(s) d'une superficie de %s mc d'appartenance %s et de type %s" % (self.getType(), self.id, self.rue, self.codePostal, self.nombrePiece, self.superficie, self.getPublic(),self.typeBatiment)

    def getPublic(self):
        if self.estPublic:
            return "publique"
        else :
            return "privee"

    def getType(self):
        if self.estAppartement:
            return "appartement"
        elif self.typeBatiment == "habitation":
            return "maison"
        else :
            return self.typeBatiment







# -------- DataBase DEFINITION -------------------------------------------------------------------------------------------------------------------------------------

batimentsDataBase = [
    #Batiments prives
    {'id':0, 'rue':'rue des chats', 'codePostal':'75003', 'nombrePiece':6, 'estAppartement':False, 'estPublic':False, 'superficie':80,'typeBatiment':"habitation" },
    {'id':1, 'rue':'avenue des chiens', 'codePostal':'75004', 'nombrePiece':2, 'estAppartement':True, 'estPublic':False, 'superficie':40,'typeBatiment':"habitation" },
    {'id':2, 'rue':'rue des colibris', 'codePostal':'75011', 'nombrePiece':1, 'estAppartement':True, 'estPublic':False, 'superficie':20,'typeBatiment':"habitation" },
    {'id':3, 'rue':'boulevard des insectes', 'codePostal':'75002', 'nombrePiece':8, 'estAppartement':False, 'estPublic':False, 'superficie':200,'typeBatiment':"habitation" },
    {'id':4, 'rue':'impasse du ciel', 'codePostal':'75018', 'nombrePiece':5, 'estAppartement':False, 'estPublic':False, 'superficie':145,'typeBatiment':"habitation" },
    {'id':5, 'rue':'rue des bourgeois', 'codePostal':'75001', 'nombrePiece':3, 'estAppartement':True, 'estPublic':False, 'superficie':45,'typeBatiment':"habitation" },
    #Batiments publiques
    {'id':6, 'rue':'rue des ecoles', 'codePostal':'75001', 'nombrePiece':15, 'estAppartement':False, 'estPublic':True, 'superficie':250,'typeBatiment':"ecole" },
    {'id':7, 'rue':'rue des commerces', 'codePostal':'75002', 'nombrePiece':3, 'estAppartement':False, 'estPublic':True, 'superficie':100,'typeBatiment':"Alimentation" },
    {'id':8, 'rue':'rue des pharmacies', 'codePostal':'75003', 'nombrePiece':4, 'estAppartement':False, 'estPublic':True, 'superficie':50,'typeBatiment':"pharmacie" },
    {'id':9, 'rue':'rue des grand commerces', 'codePostal':'75001', 'nombrePiece':5, 'estAppartement':False, 'estPublic':True, 'superficie':175,'typeBatiment':"Alimentation" }
]


habitantsDataBase = [
      {'nom':'Smith', 'prenom':'John', 'habitations':[3], 'age':54, 'nbVoitures':0, 'estHomme':True, 'estProprietaire':True},
      {'nom':'Smith', 'prenom':'Paulette', 'habitations':[3], 'age':52, 'nbVoitures':1, 'estHomme':False, 'estProprietaire':False},
      {'nom':'Smith', 'prenom':'Juniorette', 'habitations':[3], 'age':7, 'nbVoitures':0, 'estHomme':False, 'estProprietaire':False},

      {'nom':'Amstrong', 'prenom':'Sylvie', 'habitations':[5], 'age':25, 'nbVoitures':0, 'estHomme':False, 'estProprietaire':False},
      {'nom':'Amstrong', 'prenom':'Paul', 'habitations':[5], 'age':27, 'nbVoitures':0, 'estHomme':True, 'estProprietaire':False},

      {'nom':'Cresus', 'prenom':'Richard', 'habitations':[2,4], 'age':75, 'nbVoitures':3, 'estHomme':True, 'estProprietaire':True},

      {'nom':'HappyFamilly', 'prenom':'Papou', 'habitations':[1,0], 'age':45, 'nbVoitures':1, 'estHomme':True, 'estProprietaire':False},
      {'nom':'HappyFamilly', 'prenom':'Mamou', 'habitations':[1,0], 'age':47, 'nbVoitures':0, 'estHomme':False, 'estProprietaire':True},
      {'nom':'HappyFamilly', 'prenom':'Petite', 'habitations':[1,0], 'age':12, 'nbVoitures':0, 'estHomme':False, 'estProprietaire':False},
      {'nom':'HappyFamilly', 'prenom':'Petit', 'habitations':[1,0], 'age':14, 'nbVoitures':0, 'estHomme':True, 'estProprietaire':False}
]

superficieReferenceCommune = 10



# -------- DataBase Fonctions ---------------------------------------------------------------------------

def loadBatimentFromDB(dataFromDb):
    liste = []
    for data in dataFromDb:
        batiment = Batiment(data["id"],data["rue"],data["codePostal"],data["nombrePiece"],data["estAppartement"],data["estPublic"],data["superficie"],data["typeBatiment"])
        liste.append(batiment)
    return liste


def loadHabitantFromDB(dataFromDb):
    liste = []
    for data in dataFromDb:
        habitant = Habitant(data["nom"],data["prenom"],data["habitations"],data["age"],data["nbVoitures"],data["estHomme"],data["estProprietaire"])
        liste.append(habitant)
    return liste







# -------- Data Affichage -------------------------------------------------------------------------------

def demandeChiffre():
    number = None

    while number == None:
        try:
           userInput = input()
           number = int(userInput)
           if number <=0 :
               number = None
               print("Merci de fournir un entier positif superieur a 0")
           else :
                return number
        except ValueError:
           print("Merci de saisir un chiffre uniquement")

# ======> Par habitant
def getChildren(habitants,enfant):
    if enfant:
        return filter(lambda h: h.age < 18, habitants)
    else:
        return filter(lambda h: h.age >= 18, habitants)

def getLocataire(habitants,proprietaire):
    return filter(lambda h: h.estProprietaire != proprietaire, habitants)

def getMinVoiture(habitants):
    print("Nombre de voiture minimale ?")
    minVoiture = demandeChiffre()
    return filter(lambda h: h.nbVoitures>=minVoiture, habitants)

def getSex(habitants,male):
    return filter(lambda h: h.estHomme == male, habitants)

def getHabitantSpace(habitants,batiments):
    print("Surface minimale ?")
    minSurface = demandeChiffre()
    return filter(lambda h: any(b.superficie > minSurface for b in h.getHabitationsBatiment(batiments) ), habitants)


# ======> Par batiment
def getPublicBatiment(batiments,public):
    return  filter(lambda b: b.estPublic == public, batiments)

def getMinSufarceBatiment(batiments):
    print("Surface minimale ?")
    minSurface = demandeChiffre()
    return filter(lambda b: b.superficie>=minSurface, batiments)







# -------- Interface utilisateur helper -----------------------------------------------------------------------------------------

def getDataParCategorie(habitants,batiments,categorie):
    if categorie == "tout habitant":
        return habitants

    elif categorie == "tout batiment":
        return batiments

    elif categorie == "enfant":
        return getChildren(habitants,True)

    elif categorie == "adultes":
        return getChildren(habitants,False)

    elif categorie == "locataires":
        return getLocataire(habitants,True)

    elif categorie == "proprietaire":
        return getLocataire(habitants,False)

    elif categorie == "nombre voiture minimale":
        return getMinVoiture(habitants)

    elif categorie == "homme":
        return getSex(habitants,True)

    elif categorie == "femme":
        return getSex(habitants,False)

    elif categorie == "habitant avec surface minimale":
        return getHabitantSpace(habitants,batiments)

    elif categorie == "batiment public":
        return getPublicBatiment(batiments,True)

    elif categorie == "batiment prive":
        return getPublicBatiment(batiments,False)

    elif categorie == "batiment surface minimale":
        return getMinSufarceBatiment(batiments)



def AfficherDataParCategorie(habitants,batiments,categorie):
    print("=========================================================")
    for objet in getDataParCategorie(habitants,batiments,categorie): 
        print("\n- %s"%objet)
        if objet.__class__.__name__ == "Habitant":
             for h in objet.getHabitationsBatiment(batiments):  print("==> %s"%h)
    print("=========================================================")

def AfficherHabitantsParFamille(habitants):
   grouped  = defaultdict(list)
   for item in habitants:
        grouped[item.nom].append(item)

   for nomFamille, membres in grouped.items():
       print("\n======= Famille %s =========="%nomFamille)
       for m in membres: print("- %s"%m)






# -------- Simmulateur de ville-----------------------------------------------------------------------------------------

def mainSimmulation(habitants,batiments):
    print("Merci de fournir la surface de la ville a simuler")
    surface = demandeChiffre()
    print("Merci de fournir la surface minimale a assigner a chaque resident")
    surfaceParHabitant = demandeChiffre()

    multiplicateur = ceil(surface / superficieReferenceCommune)

    if multiplicateur <= 0: 
       multiplicateur = 1

    maxHabitant = ceil(surface / surfaceParHabitant)

    famillesSimmulees = simulerFamilles(habitants,multiplicateur,maxHabitant)
    AlimentationNombreSimmulee = simulerAlimentation(batiments,habitants,famillesSimmulees)
    pharmacieSimmullee = simulerPharmacie(famillesSimmulees)
    parkingSimulee = simulerParking(famillesSimmulees)
    ecoleSimulee = simulerEcole(batiments,habitants,famillesSimmulees)

    print("===================== SIMMULATION ==========================")

    print("===================== Affichage des liens familiaux possibles =====================")
    AfficherHabitantsParFamille(famillesSimmulees)

    print("\n\n\n\n\n===================== Affichage des resultats =====================")

    print("\nSurface demandee: %s"%surface)
    print("Surface par habitant demandee: %s"%surfaceParHabitant)
    print("\n Nombre de nouveaux habitants estimes: %s"%len(famillesSimmulees))
    print("\n Nombre de magasin d'Alimentation estimes: %s"%AlimentationNombreSimmulee)
    print("\n Nombre de pharmacie estimees: %s"%pharmacieSimmullee)
    print("\n Nombre d'ecole estimees: %s"%ecoleSimulee)
    print("\n Nombre de parking necessaire: %s"%parkingSimulee)
    print("\n\n")

    print("\n\n\n===================== FIN SIMMULATION ==========================")



def simulerFamilles(habitants,multiplicateur,maxHabitants):
   
   habitantsSimul = []

   grouped  = defaultdict(list)
   for item in habitants:
        grouped[item.nom].append(item)

    #On scan chaque modele de famille a partir du modele
   for nomFamille, membres in grouped.items():
       #On recopie la famille autant de fois que necessaire par rapport a la superficie fournie
       for x in range(multiplicateur):
           #On recreer des membres de familles sur le modele de famille en cours
           for m in membres: habitantsSimul.append(Habitant((m.nom+str(x)+"-SIMULATED"),m.prenom,[],m.age,m.nbVoitures,m.estHomme,m.estProprietaire))

   return habitantsSimul[0:maxHabitants]

#On calcule le nombre d'Alimentaiton necessaire par habitants en rapport avec les donnees d'entree
def simulerAlimentation(batiments,habitants,newHabitants):
    baseAlimentationNumber = list(filter(lambda b: b.typeBatiment == "Alimentation", batiments))

    return ceil(len(baseAlimentationNumber)*len(newHabitants)/len(habitants))

#On calcule le nombre d'ecole necessaire en se basant sur le ratio d'ecole par enfant a partir de la DB
def simulerEcole(batiments,habitants,newHabitants):
    oldChildNumber = len(list(filter(lambda h: h.age < 18, habitants)))
    newChildNumber = len(list(filter(lambda h: h.age < 18, newHabitants)))
    oldSchoolNumber = len(list(filter(lambda b: b.typeBatiment == "ecole", batiments)))
    return ceil(newChildNumber*oldSchoolNumber/oldChildNumber)


#On calcule le nombre de pharmacie necessaire plus la population est vieille, plus ne nombre de pharmacie augmente exponentiellement
def simulerPharmacie(newHabitants):
     totalAge = 0
     for m in newHabitants:totalAge += m.age

     moyenneAge = totalAge/len(newHabitants)
     return ceil(0.002*moyenneAge**2)

#On calcule le nombre de parking en rapport avec le nombre de voiture
def simulerParking(newHabitants):
    voitureParParking = 10

    totalVoiture = 0
    for m in newHabitants:totalVoiture += m.nbVoitures
    return ceil(totalVoiture/voitureParParking)






# -------- Chargement Base de Donnee -----------------------------------------------------------------------------------------

habitants = loadHabitantFromDB(habitantsDataBase)
batiments = loadBatimentFromDB(batimentsDataBase)







# ----------- Menus utilisateurs ---------------------------------------------------------------------------------------------------
mainMenuChoice = ["afficherdb","simuler","quitter"]
afficherDatabaseChoice = ["tout habitant","tout batiment","enfant","adultes","locataires","proprietaire","nombre voiture minimale","homme","femme","habitant avec surface minimale","batiment public","batiment prive","batiment surface minimale"]


def menuAffichageDB():
     subActionAfficher = None
     while subActionAfficher  == None:
        print("\n[DATABASE] Veuillez saisir une categorie a afficher (tappez n'importe quoi pour afficher la liste des possibles)")
        selected = input()

        if(selected.lower() in afficherDatabaseChoice):
           subActionAfficher = selected.lower()
           AfficherDataParCategorie(habitants,batiments,subActionAfficher)
        else:
            print("Cette categorie n'existe pas, liste des categories disponibles")
            for a in afficherDatabaseChoice: print("- %s"%a)

def menuAfficherMainMenu():
    mainAction = None
    while mainAction  == None:
        print("\n[PRINCIPAL] Veuillez saisir une action principale (tappez n'importe quoi pour afficher la liste des possibles)")
        selected = input()

        if(selected.lower() in mainMenuChoice):
           mainAction = selected.lower()
        else:
            print("Cette action principale n'existe pas, les actions possible ici sont:")
            for a in mainMenuChoice: print("- %s"%a)

    if mainAction == "afficherdb":
        menuAffichageDB()
    elif mainAction == "simuler":
        mainSimmulation(habitants,batiments)
    elif mainAction == "quitter":
        quit()

#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
# ----------- Debut du programme -----------------------------------------------------------------------------------------------------------------

while True:
    menuAfficherMainMenu()